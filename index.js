const criarEstrutura = () => {
  const main = document.getElementById("main");
  const secaoPino1 = document.createElement("section");
  const secaoPino2 = document.createElement("section");
  const secaoPino3 = document.createElement("section");
  const disco1 = document.createElement("div");
  const disco2 = document.createElement("div");
  const disco3 = document.createElement("div");
  const disco4 = document.createElement("div");
  const pinoDefault1 = document.createElement("div");
  const pinoDefault2 = document.createElement("div");
  const pinoDefault3 = document.createElement("div");

  pinoDefault1.className = "default";
  pinoDefault2.className = "default";
  pinoDefault3.className = "default";

  disco1.className = "disco1";
  disco2.className = "disco2";
  disco3.className = "disco3";
  disco4.className = "disco4";

  secaoPino1.appendChild(pinoDefault1);
  secaoPino2.appendChild(pinoDefault2);
  secaoPino3.appendChild(pinoDefault3);
  secaoPino1.appendChild(disco1);
  secaoPino1.appendChild(disco2);
  secaoPino1.appendChild(disco3);
  secaoPino1.appendChild(disco4);
  secaoPino1.className = "primeiroPino";
  secaoPino2.className = "segundoPino";
  secaoPino3.className = "terceiroPino";

  main.appendChild(secaoPino1);
  main.appendChild(secaoPino2);
  main.appendChild(secaoPino3);

  const msg = document.getElementById("mensagem");
  const modo = document.getElementById("modo");
  const secaoContador = document.getElementById("contador");
  let pinoDefault = secaoPino1.firstElementChild;

  let discoAtual = disco4;
  let inserindo = false;
  let contador = 0;

  const movimenta = (pino) => {
    msg.style.background = "none";
    msg.innerText = "";
    if (inserindo) {
      modo.innerHTML = `<h2>MODO: Selecionando</h2>`;
      if (
        !pino.hasChildNodes() ||
        discoAtual.clientWidth < pino.lastElementChild.clientWidth
      ) {
        pino.appendChild(discoAtual);
        contador++;
        secaoContador.innerHTML = `<h1>CONTADOR: ${contador}</h1>`;
        inserindo = false;
      } else {
        inserindo = false;
        if(discoAtual.clientWidth === pino.lastElementChild.clientWidth){
          msg.innerHTML = "<h2>O disco selecionado não e válido.</h2>";
          msg.style.background = "#ff5a5f"
        }else{
          msg.innerHTML = "<h2>O disco é maior do que o último disco da torre.</h2>";
          msg.style.background = "#ff5a5f"
        }
      }
      if (
        pino.childElementCount > 4 &&
        pino.firstElementChild !== pinoDefault
      ) {
        msg.innerHTML = "<h2>Parabéns você acertou!</h2>";
        msg.style.background = "#26C485";
        contador = 0;
        pinoDefault = pino.firstElementChild;
      }
    } else {
      modo.innerHTML = `<h2>MODO: Inserindo</h2>`;
      discoAtual = pino.lastElementChild;
      inserindo = true;
    }
  };

  secaoPino1.addEventListener("click", (e) => {
    movimenta(e.currentTarget);
  });

  secaoPino2.addEventListener("click", (e) => {
    movimenta(e.currentTarget);
  });

  secaoPino3.addEventListener("click", (e) => {
    movimenta(e.currentTarget);
  });
};
criarEstrutura();
